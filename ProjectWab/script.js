let menu = document.querySelector('#menu-bars');
let navbar = document.querySelector('.navbar');

menu.onclick = () => {
    menu.classList.toggle('fa-times');
    navbar.classList.toggle('active');
}

let section = document.querySelectorAll('section');
let navLinks = document.querySelectorAll('header .navbar a');

window.onscroll = () => {
    menu.classList.remove('fa-times');
    navbar.classList.remove('active');

    section.forEach(sec =>{
        let top = window.scrollY;
        let height = sec.offsetHeight;
        let offset = sec.offsetTop - 150;
        let id = sec.getAttribute('id');

        if(top => offset && top < offset + height){
            navLinks.forEach(links =>{
                links.classList.remove('active');
                navLinks = document.querySelector('header .navbar a[href*='+id+']').classList.add('active');
            });
        };
    });
}

// //Validate form inputs before submiting data
// function validateForm() {
//     var image = document.getElementById("image").value;
//     var price = document.getElementById("price").value;
//     var metal = document.getElementById("metal").value;
//     var diameter = document.getElementById("diameter").value;
//     var weight = document.getElementById("weight").value;
//     var note = document.getElementById("note").value;

//     if(image == ""){
//         alert("image is required");
//         return false;
//     }

//     if(price == ""){
//         alert("price is required");
//         return false;
//     }

//     if(metal == ""){
//         alert("metal is required");
//         return false;
//     }

//     if(diameter == ""){
//         alert("diameter is required");
//         return false;
//     }

//     if(weight == ""){
//         alert("weight is required");
//         return false;
//     }
//     return true;
// }

// // function to show data
// function showData(){
//     var peopleList;
//     if(localStorage.getItem("peopleList") == null) {
//         peopleList = [];
//     }
//     else{
//         peopleList = JSON.parse(localStorage.getItem("peopleList"));
//     }
//     var html = "";
//     peopleList.forEach(function(element, index){
//         html += "<tr>";
//         html += "<td>" + element.image + "</td>";
//         html += "<td>" + element.price + "</td>";
//         html += "<td>" + element.metal + "</td>";
//         html += "<td>" + element.diameter + "</td>";
//         html += "<td>" + element.weight + "</td>";
//         html += "<td>" + element.note + "</td>";
//         html += '<td><button onclick="deleteData('+index+')" class="btn btn-danger">Delete</button> <button onclick="updateData('+index+')" class="btn btn-warning m-2">Delete</button></td>';
//         html += "</tr>";

//     });

//     document.querySelector("#crudTable tbody").innerHTML = html;

// }

// //Loads All data when document or page loaded
// document.onload = showData();

// //function to add data
// function AddData(){
//     // if form is validate
//     if(validateForm() == true){
//         var image = document.getElementById("image").value;
//         var price = document.getElementById("price").value;
//         var metal = document.getElementById("metal").value;
//         var diameter = document.getElementById("diameter").value;
//         var weight = document.getElementById("weight").value;
//         var note = document.getElementById("note").value;
    
//         var peopleList;
//         if(localStorage.getItem("peopleList") == null) {
//         peopleList = [];
//         }
//         else{
//             peopleList = JSON.parse(localStorage.getItem("peopleList"));
//         }

//         peopleList.push({
//             image : image,
//             price : price,
//             metal : metal,
//             diameter : diameter,
//             weight : weight,
//             note : note,
//         });
        
//         // alert(peopleList);
//         localStorage.setItem("peopleList",JSON.stringify(peopleList));
//         showData();
//         image = document.getElementById("image").value = "";
//         price = document.getElementById("price").value = "";
//         metal = document.getElementById("metal").value = "";
//         diameter = document.getElementById("diameter").value = "";
//         weight = document.getElementById("weight").value = "";
//         note = document.getElementById("note").value = "";
//     }
// }

var selectedRow = null;
// show alerts
function showAlert(message,className){
    const div = document.createElement("div");
    div.className = `alert alert-${className}`;

    div.appendChild(document.createTextNode(message));
    const container = document.querySelector(".container");
    const main = document.querySelector(".main");
    container.insertBefore(div , main);

    setTimeout(() => document.querySelector(".alert").remove(), 3000);
}

// Add Data
document.querySelector("#student-form").addEventListener("submit", (e) => {
    e.preventDefault();

    //get form values
    const image = document.getElementById("image").value;
    const price = document.getElementById("price").value;
    const metal = document.getElementById("metal").value;
    const diameter = document.getElementById("diameter").value;
    const weight = document.getElementById("weight").value;
    const note = document.getElementById("note").value;

    //validate
    if(image == "" || price =="" || metal == "" || diameter =="" || weight == "" || note == ""){
        showAlert("Please fill in all fields","danger");
    }
    else{
        if(selectedRow == null){
            const list = document.querySelector("#student-list");
            const row = document.createElement("tr");

            row.innerHTML = `
                <td>${[image]}</td>
                <td>${price}</td>
                <td>${metal}</td>
                <td>${diameter}</td>
                <td>${weight}</td>
                <td>${note}</td>
                <td>
                
                <a href="#" class="btn btn-danger btn-sm delete">Delete</a>
            `;
            list.appendChild(row);
            selectedRow = null;
            showAlert("student Added","success");
        }
        else{
            selectedRow.children[0].textContent = image;
            selectedRow.children[1].textContent = price;
            selectedRow.children[2].textContent = metal;
            selectedRow.children[3].textContent = diameter;
            selectedRow.children[4].textContent = weight;
            selectedRow.children[5].textContent = note;
            selectedRow = null;
            showAlert("Student Info Edited","info")
        }

        clearFields();
    }
});

// Edit Data
// document.querySelector("#student-list").addEventListener("click",(e) => {
//     target = e.target;
//     if(target.classList.contains("edit")){
//         selectedRow = target.parentElement.parentElement;
//         document.getElementById("#image").value = selectedRow.children[0].textContent = image;
//         document.getElementById("#price").value = selectedRow.children[1].textContent = price;
//         document.getElementById("#metal").value = selectedRow.children[2].textContent = metal;
//         document.getElementById("#diameter").value = selectedRow.children[3].textContent = diameter;
//         document.getElementById("#weight").value = selectedRow.children[4].textContent = weight;
//         document.getElementById("#note").value = selectedRow.children[5].textContent = note;
//     }
// });


// Delete Data
document.querySelector("#student-list").addEventListener("click",(e) => {
    target = e.target;
    if(target.classList.contains("delete")){
        target.parentElement.parentElement.remove();
        showAlert("Student Data Delete", "danger")
    }

});

document.querySelector('#search-icon').onclick = () => {
    document.querySelector('#search-form').classList.toggle('active');
};

document.querySelector('#search-close').onclick = () => {
    document.querySelector('#search-form').classList.remove('active');
};

document.querySelector('#request-icon').onclick = () => {
    document.querySelector('#request-form').classList.toggle('active');
};

document.querySelector('#request-close').onclick = () => {
    document.querySelector('#request-form').classList.remove('active');
};
